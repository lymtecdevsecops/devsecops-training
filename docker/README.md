# Some tips about Docker

![Easy Docker](https://www.docker.com/sites/default/files/d8/styles/medium/public/2020-01/Octopus_moby_800%20%281%29.png)




Docker is a solution that uses containers to run different types of applications and plataforms. With container it is possible to upload applications quickly, allowing developers to work more efficiently  and system administrators to make applications available quickly, safely and scalably.


Docker é uma solução que utiliza containers para rodar diversos tipos de aplicações. Com container é possível subir aplicações rapidamente permitindo ao desenvolvedor um trabalho mais produtivo e ao administrator disponilizar aplicações de forma rápida, segura e escalável.


## Get started with Docker
    
    https://www.docker.com
    https://www.docker.com/get-started


## Container

    https://www.docker.com/resources/what-container
    https://www.docker.com/101-tutorial

## Docker Community

    https://www.docker.com/docker-community

## Documentation

    https://docs.docker.com/
      
## Basic Steps

1) Install Docker

    # Choose best installation according your OS - https://www.docker.com/get-started

2) Run a simple docker image

    # docker run hello-world
    # docker run -it ubuntu bash

3) Create images with Dockerfile

3.1) Create a directory

    # mkdir docker-img

3.2) Move to directory

    # cd docker-img

3.3) Create a Dockerfile  

    # vi Dockerfile

3.4) Add the following lines (Sample)

    # Dockerfile
    FROM nginx
    WORKDIR /app 
    EXPOSE 80


3.5) Build you image 

    # docker build -t nginx-test .

3.6) Rum image

    # docker run -p 80:80 -d nginx-test 

3.7) Test your image using curl, wget ou a browser

    # wget http://localhost
    
3.8) Test your code

    # Try it