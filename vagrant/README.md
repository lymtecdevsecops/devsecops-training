This repos contains samples for Vagrant

# Vagrant

## Introduction to Vagrant

Vagrant is a tool for building and managing virtual machine environments in a single workflow. 

Create a Vagrantfile and run commands to deploy

### Basic commands
	
	vagrant init
	vagrant up
	vagrant halt
	vagrant destroy
	vagrant ssh

### Documentation

	https://www.vagrantup.com/
	https://www.vagrantup.com/intro/index.html
