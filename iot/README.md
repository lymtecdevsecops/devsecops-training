# Learning IoT
This repository explain basic concepts of IoT


## Basic
	
	How does IoT work? 
	
	https://www.arduino.cc/
	https://www.raspberrypi.org/
	
	


Como funciona a [Internet das Coisas](https://www.youtube.com/watch?v=jlkvzcG1UMk "IoT") - NIC.br Youtube.

Apresentação de IoT: https://www.youtube.com/watch?v=s4DUFhNexbU

	
## Intermediate

	Using Node-RED and HiveMQ
	
	https://www.hivemq.com/
	https://nodered.org/
	
## Advanced
	
	Using Cloud Services and Developing IoT Applications and Platforms
	
	https://tago.io/
	
	https://thingsboard.io/
	http://www.dojot.com.br/
	
	
Apresentação sobre IoT - [TDC POA 2019](https://s3-sa-east-1.amazonaws.com/thedevconf/presentations/TDC2019POA/iot/HFK-2295_2019-11-30T020327_TDC_POA_2019_Leonardo_IoT.pdf "TDC POA 2019").

Apresentação sobre ChatBot - [TDC POA 2019](https://s3-sa-east-1.amazonaws.com/thedevconf/presentations/TDC2019POA/chatbots/OJD-9519_2019-11-30T020304_TDC_POA_2019_Leonardo_ChatBot.pdf "TDC POA 2019").

	
	

## @LYMTEC

Let us know if you have questions or comments. Hope you Enjoy IT. 

Thanks!!!