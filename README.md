# DevSecOps-Training

DevSecOps-Training Repository

This repository contains useful informations for DevOps professionals.

	### Learn about Markdown
	https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html


## Provisioning your infrastructure

Creating Virtual Machines and Infrastrucuture with Terraform and Vagrant

	Terraform
	https://www.terraform.io/
	https://www.terraform.io/docs/providers/index.html
	
	Kubernetes
	https://www.terraform.io/docs/providers/kubernetes/index.html
	https://www.terraform.io/docs/providers/kubernetes/guides/getting-started.html

	Vagrant
	https://www.vagrantup.com/
	https://app.vagrantup.com/bento/boxes/ubuntu-18.04
	https://www.vagrantup.com/docs/vagrantfile/machine_settings.html#config-vm-hostname
	
	Repository
	https://bitbucket.org/lymtecdevsecops/devsecops-training/src/master/terraform/
	

## Configuring Servers

Installing and configuring VMs with Ansible

	https://docs.ansible.com
	https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html#playbook-language-example
	https://docs.ansible.com/ansible/latest/modules/shell_module.html?highlight=shell
	https://docs.ansible.com/ansible/latest/modules/k8s_module.html
	https://www.ansible.com/blog/continuous-improvements-in-ansible-and-kubernetes-automation
	https://docs.ansible.com/ansible/latest/modules/file_module.html
	https://docs.ansible.com/ansible/latest/modules/copy_module.html
	https://docs.ansible.com/ansible/latest/modules/service_module.html
	https://docs.ansible.com/ansible/latest/modules/yum_module.html
	https://docs.ansible.com/ansible/latest/modules/rpm_key_module.html
	https://docs.ansible.com/ansible/latest/modules/file_module.html
	https://docs.ansible.com/ansible/latest/modules/unarchive_module.html
	https://docs.ansible.com/ansible/latest/user_guide/playbooks_error_handling.html
	https://docs.ansible.com/ansible/latest/modules/shell_module.html

[Go to Repo:](https://bitbucket.org/lymtecdevsecops/devsecops-training/src/master/ansible/ "Ansible")



## Monitoring

Monitoring Servers and Aplications With Prometheus

	https://prometheus.io/
	https://prometheus.io/download/
	https://prometheus.io/docs/prometheus/latest/getting_started/
	https://prometheus.io/docs/introduction/overview/
	https://prometheus.io/docs/instrumenting/exporters/
	https://prometheus.io/docs/instrumenting/writing_clientlibs/
	https://github.com/prometheus/alertmanager/releases
	https://docs.docker.com/config/thirdparty/prometheus/
	https://cloud.google.com/monitoring/kubernetes-engine/prometheus?hl=pt-br
	https://prometheus.io/docs/prometheus/latest/getting_started/
	https://prometheus.io/docs/instrumenting/exposition_formats/
	https://prometheus.io/docs/prometheus/latest/management_api/
	https://github.com/prometheus/node_exporter
	https://github.com/prometheus/client_java/tree/master/simpleclient/src/main/java/io/prometheus/client
	https://prometheus.io/docs/guides/node-exporter/
	https://awesome-prometheus-alerts.grep.to/rules.html

	Micrometer
	https://micrometer.io/docs/registry/prometheus
	https://mvnrepository.com/artifact/io.micrometer/micrometer-registry-prometheus

## Logging

Logging with Elastic Stack

	https://www.elastic.co/pt/
	https://www.elastic.co/downloads/elasticsearch
	https://www.elastic.co/downloads/kibana
	
	SOLR
	http://lucene.apache.org/solr/quickstart.html
	http://www-eu.apache.org/dist/lucene/solr/6.6.0/solr-6.6.0.tgz

## Reporting

Creating Dashboards with Grafana

	https://grafana.com/
	https://grafana.com/get
	https://grafana.com/docs/grafana/latest/reference/export_import/
	
	Nodered
	https://nodered.org/

## Performance 

Monitor application Performance with New Relic

	https://newrelic.com/
	
Testing tools
	
	Jmeter
	Tsung

## Container

Working With Docker

	https://www.docker.com/
	https://hub.docker.com/
	https://www.docker.com/get-started
	https://docs.docker.com/compose/
	https://docs.docker.com/engine/reference/commandline/exec/
	https://docs.docker.com/engine/reference/commandline/run/
	https://docs.docker.com/engine/reference/builder/
	https://docs.docker.com/compose/env-file/
	
Creating Docker in Heroku

	https://devcenter.heroku.com/articles/container-registry-and-runtime
	https://github.com/heroku/alpinehelloworld
	
NodeJS
	
	https://nodejs.org/de/docs/guides/nodejs-docker-webapp/
	
Spring

	https://spring.io/guides/gs/spring-boot-docker/

Wildfly

	https://hub.docker.com/r/jboss/wildfly/dockerfile
	https://hub.docker.com/r/jboss/wildfly/
	https://developers.redhat.com/jboss-docker/

Java
	
	https://runnable.com/docker/java/dockerize-your-java-application
	https://hub.docker.com/_/openjdk

Postgresql
	
	https://docs.docker.com/engine/examples/postgresql_service/
	

## Container management

Working With Kubernetes

	https://github.com/redhat-nfvpe/kube-ansible
	https://www.digitalocean.com/community/tutorials/como-criar-um-cluster-kubernetes-1-10-usando-kubeadm-no-centos-7-pt
	https://www.digitalocean.com/community/tutorials/como-criar-um-cluster-kubernetes-1-11-usando-kubeadm-no-ubuntu-18-04-pt

	Heroku
	Openshift
	
	https://confluence.atlassian.com/bitbucket/deploy-to-heroku-872013667.html
	https://bitbucket.org/bitbucketpipelines/example-heroku-deploy-spring-boot/src/master/bitbucket-pipelines.yml
	https://devcenter.heroku.com/articles/deploying-nodejs
	https://help.heroku.com/P1AVPANS/why-is-my-node-js-app-crashing-with-an-r10-error
	https://medium.com/code-prestige/deployando-seu-projeto-em-node-js-no-heroku-b49a6ae7dbc3

# Communication

	Slack
	https://api.slack.com/messaging/webhooks
	
# Developing Applications

	Node JS
	https://nodejs.org/en/
	https://nodejs.org
	https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions-enterprise-linux-fedora-and-snap-packages
	https://github.com/nodesource/distributions/blob/master/README.md
	https://expressjs.com/pt-br/
	https://expressjs.com/en/resources/middleware/cors.html
	
	Angular
	https://coreui.io/angular/
	https://spring.io/guides/gs/consuming-rest-angularjs/

	Java - Spring Boot
	CORS
	https://spring.io/guides/gs/rest-service-cors/
	Docker
	https://spring.io/guides/gs/spring-boot-docker/
	K8s
	https://spring.io/guides/gs/spring-boot-kubernetes/

	DotNet Core
	https://docs.microsoft.com/pt-br/dotnet/core/install/sdk?pivots=os-linux
	https://docs.microsoft.com/pt-br/dotnet/core/install/linux-package-manager-ubuntu-1804

	Python
	https://www.w3schools.com/python/default.asp
	https://www.python.org/
	https://palletsprojects.com/p/flask/
	
	Python - Flask
	https://flask-cors.readthedocs.io/en/latest/
	
	Spring Cloud
	https://cloud.spring.io/spring-cloud-netflix/
	https://github.com/spring-cloud/spring-cloud-netflix
	https://spring.io/blog/2015/01/20/microservice-registration-and-discovery-with-spring-cloud-and-netflix-s-eureka

	Quick start
	https://projects.spring.io/spring-boot/#quick-start
	https://spring.io/guides/gs/rest-service/#_make_the_application_executable

	Eureka
	https://spring.io/guides/gs/service-registration-and-discovery/

	Config
	https://spring.io/guides/gs/centralized-configuration/
	https://cloud.spring.io/spring-cloud-config/
	https://spring.io/guidhttps://docs.microsoft.com/en-us/aspnet/core/getting-started/?view=aspnetcore-3.1&tabs=windowss/gs/rest-service-cors/
	
	dotnet core
	https://docs.microsoft.com/en-us/aspnet/core/getting-started/?view=aspnetcore-3.1&tabs=windows
	https://dotnet.microsoft.com/download/dotnet-core/3.1
	https://docs.microsoft.com/pt-br/dotnet/core/install/linux-package-manager-ubuntu-1904
	https://hub.docker.com/_/microsoft-dotnet-core-samples/
	https://docs.microsoft.com/pt-br/aspnet/core/?view=aspnetcore-3.1
	https://docs.docker.com/engine/examples/dotnetcore/
	https://docs.microsoft.com/pt-br/dotnet/core/docker/build-container
	https://docs.microsoft.com/pt-br/aspnet/core/host-and-deploy/docker/building-net-docker-images?view=aspnetcore-3.1
	
	Installing Dotnet core
	https://docs.microsoft.com/pt-br/dotnet/core/install/sdk?pivots=os-linux#all-net-core-downloads
	https://dotnet.microsoft.com/learn/dotnet/hello-world-tutorial/intro?sdk-installed=true
	https://medium.com/@renatoluizcarvalho/realizando-deploy-net-core-2-2-no-heroku-registry-5f34cbb50b07
	https://help.heroku.com/PAT3YEDU/does-heroku-support-net-applications
	https://devcenter.heroku.com/articles/build-docker-images-heroku-yml
	
# Front

	HTML
	https://getbootstrap.com/docs/4.4/layout/grid/
	https://api.jquery.com/text/
	https://www.w3schools.com/jquery/ajax_get.asp
	https://www.w3schools.com/jquery/tryit.asp?filename=tryjquery_ajax_get

	Bootstrap
	Angular
	React
	
# Data

	Redis
	https://redis.io/
	https://github.com/xetorthio/jedis
	http://download.redis.io/releases/redis-4.0.1.tar.gz
	
# Securing Applications

	https://owasp.org/
	CDN
	WAF
	NMAP

# Git
	
	GitHub
	GitLab
	BitBucket
	
# CI-CD
	
	Jenkins
	Rundeck
	Bamboo

# Cloud Services

	AWS
	Azure
	GCP
	IBM
	
# Mobile
	
	Android studio
	Ionic
	Kotlin
	
# Tools
	
	Visual Studio Code
	Atom
	Eclipse
	IntelliJ	
	
# Projects

	https://www.cncf.io/projects/
	https://landscape.cncf.io/format=card-mode&grouping=no&sort=stars
	
# Inovation
	
	IoT
	3D Printing
	IA 
	VR and VA

# Certifications

	DevOps Master
	Cloud Enginner