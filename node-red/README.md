# How to Install Node-Red

## Requerements

    - Install a Linux Server (Ubuntu)
    - Install npm and nodejs
    
## Installing and using Node-red

    - Install node-red
    sudo npm install -g --unsafe-perm node-red
    - Run node-red
    node-red

## Working with node-red

    - Access Console
    http://localhost:1880/

    - Access Dashboard
    http://localhost:1880/ui 



