This repository contains documentation and some ansible examples

# First Step

Install Ansible - https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

## For ubuntu:
	apt install ansible
	
## Copy ssh key
	* You need to generate you ssh key and copy it into your managed server prior to run ansible on it
	
	# Generate key
	ssh-keygen
	
	# send public to the managed server
    ssh-copy-id vm-poc-01
    
	# SSH connection without password
	ssh vm-poc-01

	# Create a hosts files and add the following content
	vi hosts
			
			[pocservers]
			vm-poc-01

	# test ansible command
	ansible -m ping  vm-poc-01 -i hosts

## Commands ( Create your work area)

	# Create a directory to work 
	mkdir ansible
	
	# Move to folder
	cd ansible 

## Create your first playbook 

### Playbook to install nginx 
vi web.yml
		
	- hosts: pocservers
	  become: yes
	  tasks:

	   - name: install nginx
	     apt:
	       name: nginx
	       state: present
	       update_cache: true
			

### Run playbook

	ansible-playbook web.yml  -i hosts
	
## Documentation

	https://www.ansible.com/
	https://www.ansible.com/overview/how-ansible-works
	https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html#playbook-language-example
	https://docs.ansible.com/ansible/latest/modules/shell_module.html?highlight=shell
	https://docs.ansible.com
	https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html#playbook-language-example
	https://docs.ansible.com/ansible/latest/modules/shell_module.html?highlight=shell
	https://docs.ansible.com/ansible/latest/modules/k8s_module.html
	https://www.ansible.com/blog/continuous-improvements-in-ansible-and-kubernetes-automation
	https://docs.ansible.com/ansible/latest/modules/file_module.html
	https://docs.ansible.com/ansible/latest/modules/copy_module.html
	https://docs.ansible.com/ansible/latest/modules/service_module.html
	https://docs.ansible.com/ansible/latest/modules/yum_module.html
	https://docs.ansible.com/ansible/latest/modules/rpm_key_module.html
	https://docs.ansible.com/ansible/latest/modules/file_module.html
	https://docs.ansible.com/ansible/latest/modules/unarchive_module.html
	https://docs.ansible.com/ansible/latest/user_guide/playbooks_error_handling.html
	https://docs.ansible.com/ansible/latest/modules/shell_module.html


## Challenge

	Run dev-setup.yml
