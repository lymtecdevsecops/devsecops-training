# Spring boot

## Documentation

Spring Project

	https://spring.io/
	https://spring.io/projects/spring-cloud
	
Create a project using Spring Initializr

	https://start.spring.io/
	
Hello World
	
	https://spring.io/guides/gs/rest-service/
	
Consuming Resp API
	
	https://spring.io/guides/gs/consuming-rest/
	https://spring.io/blog/2009/03/27/rest-in-spring-3-resttemplate
	
## Running

	mvn spring-boot:run
	mvn clean package



