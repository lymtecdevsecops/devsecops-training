# This repos contains samples for Terraform 

# Terraform 

## Introduction to Terraform 

Terraform is a tool for deploying managing virtual machine environments. 

Create a .tf file and run commands to deploy

### Basic commands
	
	terraform init
	terraform plan
	terraform apply
	terraform destroy 

### Documentation

	https://www.terraform.io/
	
	
![Terraform using Azure](https://lymatayoshi.bitbucket.io/imagens/fundo-dot.png)


