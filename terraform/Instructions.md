# Step by step    

## Requirements

* Create Azure Account
	https://portal.azure.com/#home
	
* We need a Linux machine (Using ubuntu)
	Install ubuntu on Linux using Microsoft Store

* Create a .gitignore sample                                                                                                     	https://github.com/hashicorp/terraform-guides/blob/master/.gitignore   

* Install the following packs
	apt install wget
	apt install unzip

* Install azure-cli 
	https://docs.microsoft.com/pt-br/cli/azure/install-azure-cli-apt?view=azure-cli-latest

	## Install using apt
	curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash

	## Run Az login
	az login

	##  Login to you Azure account
	Open your browser and log into your account
	
![Azure Portal](https://bitbucket.org/lymtecdevsecops/devsecops-training/raw/8aa38a55c09da00f543db621cb20973b4e896392/terraform/screens/tf-azure.PNG)

	

* Install Terraform

	# Download terraform

	cd
	mkdir tf 
	cd tf
	wget https://releases.hashicorp.com/terraform/0.12.23/terraform_0.12.23_linux_amd64.zip
	unzip terraform_0.12.23_linux_amd64.zip


## Working and running terraform

# Checking 
	## Check terraform version
	~/tf/terraform -v

	## Create a TF file

	For testing you can use 
	https://bitbucket.org/lymtecdevsecops/devsecops-training/src/master/terraform/azure-vm/azure-vm-01.tf

# Running terraform 
	## Create a tf file
	vi azure-vm-01.tf

	## Copy from:
        https://bitbucket.org/lymtecdevsecops/devsecops-training/src/master/terraform/vm-sample.tf

	## Install providers
	~/tf/terraform init
	
![Terraform Init](https://bitbucket.org/lymtecdevsecops/devsecops-training/raw/aede5c37f6594e192c2ee13e77aa3ece48592e1b/terraform/screens/tf-init.PNG)

	## Check plan
	~/tf/terraform plan
![Terraform Init](https://bitbucket.org/lymtecdevsecops/devsecops-training/raw/a545367bc9dd8c3bc5426cc618de9a978ec8af52/terraform/screens/tf-plan-2.PNG)

	## Deploy using terraform
	~/tf/terraform apply
![Terraform Init](https://bitbucket.org/lymtecdevsecops/devsecops-training/raw/a545367bc9dd8c3bc5426cc618de9a978ec8af52/terraform/screens/tf-apply-3.PNG)

	## Check portal azure
	Log into you Azure Portal and check your VM Created
![Terraform Init](https://bitbucket.org/lymtecdevsecops/devsecops-training/raw/a545367bc9dd8c3bc5426cc618de9a978ec8af52/terraform/screens/tf-vm.PNG)

	## Samples
	https://www.terraform.io/docs/providers/azurerm/index.html
	https://docs.microsoft.com/pt-br/azure/terraform/terraform-create-complete-vm

![Terraform using Azure](https://lymatayoshi.bitbucket.io/imagens/fundo-dot.png)



     
                                                                                                                                                                 